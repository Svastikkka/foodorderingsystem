# Food Ordering System
- A simple food ordering system where customer can choose the menu items to buy and admin can manage the food items ordered by customer. customers have create an account to purchase food item.

## Requirements:
    - Designing a real-life system, that will be used by real users.
    - The application should contain 2 types of users: Restaurants and Customers
    - Pages to be developed-
        - ‘Registration’ pages - Different registration pages for Restaurants & Customers. Capture customer’s preferences (veg/non-veg) during registration.
        - ‘Login’ pages - Single/different login pages for restaurants & customers.
        - ‘Add menu item’ page - A restaurant, once logged in, should be able to add details of new food items (including whether they are veg or non-veg) to their restaurant’s menu. Access to this page should be restricted only to restaurants.
        - ‘Menu’ page - There should be a page that displays all the available food items along with which restaurants have them and a ‘Order’ button. This page should be accessible to everyone, irrespective of whether the user is logged in or not. Expected functionality on click of the 'Order' button
        - Only customers should be able to order food by clicking the ‘Order’ button.

## Technologies:
    - Write the front-end in HTML/CSS/JS. May use Bootstrap.
    - Write the backend in either core PHP or PHP Codeigniter framework. Use MySQL as the database.
## Installation

    - Download the project
    - Run xampp server(apache, mysql)
    - import foodsys.sql in database(foodsys) through phpmyadmin
    - Run the project